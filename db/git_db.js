const vue = [
  {
    "id": 1,
    "title": "Vue #1",
    "review": "Installasi Vue dan Vuetify",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": false,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"Instalasi Vue",
        "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi Vue-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi Vue-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-3",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"Instalasi Composer",
        "link":"https://www.niagahoster.co.id/blog/cara-install-composer/",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
    ]
  },
  {
    "id": 2,
    "title": "Laravel #2",
    "review": "Sumber referensi mengenai struktur direktori dan artisan",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": true,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"Struktur Direktori Laravel",
        "link":"https://medium.com/easyread/struktur-folder-laravel-framework-299f0225cd55",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"Artisan CLI (Command Line Interface)",
        "link":"https://medium.com/easyread/apa-itu-artisan-cli-pada-laravel-62a94232a29a",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  }
]
module.exports = vue;
