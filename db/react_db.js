const react = [
  {
    "id": 1,
    "title": "React #1",
    "review": "UI library di React js",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": true,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"Material UI",
        "link":"https://material-ui.com/",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"Bootstrap-react",
        "link":"https://react-bootstrap.github.io/",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  },
  {
    "id": 2,
    "title": "React #2",
    "review": "Konsumsi API",
    "img": "https://i.pinimg.com/236x/80/e3/2d/80e32d03e650596b8f1ae56e40628ae2--indonesia-book-covers.jpg",
    "done": true,
    "accordion": 
    [
      {
        "eventKey":1,
        "item":"Cara untuk dengan Fetching API",
        "link":"https://medium.com/@muhamadirhamprasetyo/mana-yang-bagus-ya-buat-http-call-fetch-api-atau-axios-22bb8ed6b1c9",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":2,
        "item":"Cara-1 dengan Fetching API",
        "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      },
      {
        "eventKey":3,
        "item":"Cara-2 dengan Axios",
        "link":"https://www.niagahoster.co.id/blog/cara-install-composer/",
        "card":
        [
          {
            "cardtitle":"Referensi Instalasi XAMPP-1",
            "link":"https://www.niagahoster.co.id/blog/cara-menggunakan-xampp/"
          },
          {
            "cardtitle":"Referensi Instalasi XAMPP-2",
            "link":"https://www.rumahweb.com/journal/install-xampp-di-windows/"
          }
        ]
      }
    ]
  }    
]

module.exports = react;
