const express = require("express");
const app = express();
const cors = require("cors");

app.use(
  cors({ 
    origin: ["*"] 
  })
);

const laravel = require("./db/laravel_db");
const adonis = require("./db/adonis_db");
const react = require("./db/react_db");
const vue = require("./db/vue_db");
const git = require("./db/git_db");
/*
Using buildpack: heroku/nodejs
git remote -v / heroku create
git add .
git commit -am "upload"
git push heroku main or git push heroku HEAD:master
https://stackoverflow.com/questions/14322989/first-heroku-deploy-failed-error-code-h10
*/
app.get("/", (req, res) => {
  res.send("Hello World!");
});
app.get("/laravel", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.json({ laravel });
});
app.get("/adonis", (req, res) => {
  return res.json({
    adonis,
  });
});
app.get("/react", (req, res) => {
  return res.json({
    react,
  });
});
app.get("/vue", (req, res) => {
  return res.json({
    vue,
  });
});
app.get("/git", (req, res) => {
  return res.json({
    git,
  });
});
app.listen(process.env.PORT || 8000, function () {
  console.log(
    "App running on port %d in %s mode",
    this.address().port,
    app.settings.env
  );
});
